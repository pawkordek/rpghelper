package rpghelper.controller;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;
import rpghelper.model.Logger;
import rpghelper.model.Module;

import java.io.IOException;

import static com.google.common.base.Preconditions.checkNotNull;

public class ViewChanger {
    private static final org.apache.log4j.Logger logger = Logger.getInstance();

    private final double SCREEN_WIDTH = Screen.getPrimary().getBounds().getWidth();
    private final double SCREEN_HEIGHT = Screen.getPrimary().getBounds().getHeight();

    private final String VIEW_FILES_RESOURCE_FOLDER_PATH;
    private final String VIEW_FILE_EXTENSION = ".fxml";

    private FXMLLoader fxmlLoader;
    private final Stage stage;
    private final Module module;

    public ViewChanger(Stage stage, String viewFilesResourceFolderPath) {
        this(stage, Module.getEmptyModule(), viewFilesResourceFolderPath);
    }

    public ViewChanger(Node node, String viewFilesResourceFolderPath) {
        this(getStageOutOfNode(node), Module.getEmptyModule(), viewFilesResourceFolderPath);
    }

    public ViewChanger(Node node, Module module, String viewFilesResourceFolderPath) {
        this(getStageOutOfNode(node), module, viewFilesResourceFolderPath);
    }

    private static Stage getStageOutOfNode(Node node) {
        checkNotNull(node, "Node in" + ViewChanger.class + "cannot be null");
        return (Stage) node.getScene().getWindow();
    }

    public ViewChanger(Stage stage, Module module, String viewFilesResourceFolderPath) {
        this.stage = checkNotNull(stage, "Stage in " + ViewChanger.class + "retrieved from Node cannot be null");
        this.module = checkNotNull(module, "Module in " + ViewChanger.class + " cannot be null");
        VIEW_FILES_RESOURCE_FOLDER_PATH = checkNotNull(viewFilesResourceFolderPath, "View files folder path in " + ViewChanger.class + " cannot be null");
    }

    @Override
    public String toString() {
        return "ViewChanger";
    }

    public void changeViewTo(String viewName) {
        loadView(viewName);
        setUpController();
        displayView();
    }

    private void loadView(String viewName) {
        fxmlLoader = new FXMLLoader(
                getClass().getResource(VIEW_FILES_RESOURCE_FOLDER_PATH + viewName + VIEW_FILE_EXTENSION));
        try {
            fxmlLoader.load();
        } catch (IOException e) {
            logger.error("Exception during view loading", e);
        }
    }

    private void setUpController() {
        Controller currentController = fxmlLoader.getController();
        currentController.setModule(module);
        currentController.lateInitialize();
    }

    private void displayView() {
        Scene scene = createScaledSceneBasedOn(stage.getScene());
        displayViewOn(scene);
    }

    private Scene createScaledSceneBasedOn(Scene scene) {
        Parent root = fxmlLoader.getRoot();
        if (scene == null) {
            return new Scene(root, SCREEN_WIDTH, SCREEN_HEIGHT);
        } else {
            double sceneHeight = stage.getScene().getHeight();
            double sceneWidth = stage.getScene().getWidth();
            return new Scene(root, sceneWidth, sceneHeight);
        }
    }

    private void displayViewOn(Scene scene) {
        stage.setScene(scene);
        stage.show();
    }

}
