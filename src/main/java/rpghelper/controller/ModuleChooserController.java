package rpghelper.controller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import rpghelper.RpgHelper;
import rpghelper.model.Logger;
import rpghelper.model.Module;
import rpghelper.model.ModuleLoader;

import java.io.IOException;
import java.util.List;

public class ModuleChooserController implements Controller {
    private static final org.apache.log4j.Logger logger = Logger.getInstance();

    private Module module;
    private final ModuleLoader moduleLoader = new ModuleLoader("modules/");

    @FXML
    private ChoiceBox<String> moduleChooserChoiceBox;

    @FXML
    private void switchToBattle() {
        Module chosenModule = moduleLoader.getModule(moduleChooserChoiceBox.getSelectionModel().getSelectedItem());
        ViewChanger viewChanger = new ViewChanger(moduleChooserChoiceBox, chosenModule, RpgHelper.DEFAULT_VIEW_FILES_RESOURCE_FOLDER_PATH);
        viewChanger.changeViewTo("BattlePreparation");
    }

    @Override
    public void lateInitialize() {
        List<String> modulesList = getModulesListForDisplaying();
        displayModulesFrom(modulesList);
    }

    private List<String> getModulesListForDisplaying() {
        try {
            return moduleLoader.getAvailablModules();
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("There are no loadable modules. Please make sure you have modules in 'modules' folder and" +
                    "restart the application");
            alert.showAndWait();
            throw new IllegalStateException("There are no loadable modules. Application cannot continue", e);
        }
    }

    private void displayModulesFrom(List<String> modulesList) {
        moduleChooserChoiceBox.setItems(FXCollections.observableArrayList(modulesList));
        Platform.runLater(() -> moduleChooserChoiceBox.getSelectionModel().selectFirst());
    }

    @Override
    public void setModule(Module module) {
        this.module = module;
    }

    @Override
    public Module getModule() {
        return module;
    }


}
