package rpghelper.controller;

import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import rpghelper.model.*;

import java.io.IOException;
import java.util.List;

public class BattlePreparationController implements Controller {
    private static final org.apache.log4j.Logger logger = Logger.getInstance();

    private Module module;
    private String chosenUnitType;

    @FXML
    private ListView<DataObject> unitsInBattleListView;
    @FXML
    private Button addUnitToBattleButton;
    @FXML
    private TextField unitChooserTextField;
    @FXML
    private ImageView unitImageView;
    @FXML
    private ListView<String> chosenUnitStatsListView;
    @FXML
    private ListView<DataObject> chosenUnitSkillsListView;
    @FXML
    private ListView<DataObject> chosenUnitEquipmentListView;
    @FXML
    private ListView<DataObject> chosenUnitAttacksListView;
    @FXML
    private ListView<String> additionalInfoListView;
    @FXML
    private ChoiceBox<String> unitTypesChoiceBox;

    @FXML
    private void addUnitToBattle() {
        String unitName = unitChooserTextField.getText();
        DataObject unitToAdd = module.getUnit(chosenUnitType, unitName);
        if (!unitToAdd.equals(DataObject.getEmptyDataObject())) {
            unitsInBattleListView.getItems().add(unitToAdd);
        } else {
            logger.warn("Unit with a name=" + unitName + " doesn't exist.");
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Unit with a name=" + unitChooserTextField.getText() + " doesn't exist.");
            alert.showAndWait();
        }
    }

    @Override
    public void lateInitialize() {
        addListeners();
        displayUnitTypes();
    }

    private void addListeners() {
        unitsInBattleListView.getSelectionModel().selectedItemProperty().
                addListener(this::onUnitsInBattleListViewItemSelection);
        unitTypesChoiceBox.getSelectionModel().selectedItemProperty().
                addListener(this::onUnitTypesChoiceBoxItemSelection);
        chosenUnitAttacksListView.getSelectionModel().selectedItemProperty().
                addListener(this::onChosenUnitAttacksListViewItemSelection);
        chosenUnitSkillsListView.getSelectionModel().selectedItemProperty().
                addListener(this::onChosenSkillListViewItemSelection);
        chosenUnitEquipmentListView.getSelectionModel().selectedItemProperty()
                .addListener(this::onChosenUnitEquipmentListViewItemSelection);
    }

    private void displayUnitTypes() {
        ObservableList<String> observableUnitTypes = FXCollections.observableArrayList(module.getUnitTypesList());
        unitTypesChoiceBox.setItems(observableUnitTypes);
        Platform.runLater(() -> unitTypesChoiceBox.getSelectionModel().selectFirst());
    }

    private void onUnitsInBattleListViewItemSelection(ObservableValue<? extends DataObject> observable, DataObject oldChosenUnit, DataObject newChosenUnit) {
        changeUnitImageToImageOf(newChosenUnit);
        displayStatisticsOf(newChosenUnit);
        displaySkillsOf(newChosenUnit);
        displayEquipmentOf(newChosenUnit);
        displayAttacksOf(newChosenUnit);
    }

    //TODO: Possible image loader in the future
    private void changeUnitImageToImageOf(DataObject unit) {
        unitImageView.setImage(new Image("file:" + module.getFolderPath() + "images/" + unit.getStringData("image")));
    }

    private void displayStatisticsOf(DataObject unit) {
        try {
            List<String> unitDataList = new DataGetter(module).getStatisticsOf(unit);
            putListIntoListView(unitDataList, chosenUnitStatsListView);
        } catch (IOException e) {
            logger.error("Exception when trying to display unit's statistics", e);
        }
    }

    private void displaySkillsOf(DataObject unit) {
        List<DataObject> skillsList = new DataGetter(module).getSkillsListOf(unit);
        putListIntoListView(skillsList, chosenUnitSkillsListView);
    }

    private void displayEquipmentOf(DataObject unit) {
        List<DataObject> equipmentList = new DataGetter(module).getEquipmentListOf(unit);
        putListIntoListView(equipmentList, chosenUnitEquipmentListView);
    }

    private void displayAttacksOf(DataObject unit) {
        List<DataObject> attacksList = new DataGetter(module).getAttacksListOf(unit);
        putListIntoListView(attacksList, chosenUnitAttacksListView);
    }

    private void onUnitTypesChoiceBoxItemSelection(ObservableValue<? extends String> observable, String oldChosenUnitType, String newChosenUnitType) {
        chosenUnitType = newChosenUnitType;
    }

    private void onChosenUnitAttacksListViewItemSelection(ObservableValue<? extends DataObject> observable, DataObject oldChosenAttack, DataObject newChosenAttack) {
        //This null check prevents the call to DataGetter if selection of ListView was cleared and selected object became null
        if (newChosenAttack != null) {
            try {
                clearSelectionOf(chosenUnitSkillsListView, chosenUnitEquipmentListView);
                List<String> attackDataList = new DataGetter(module).getDisplayableAttackDataOf(newChosenAttack);
                putListIntoListView(attackDataList, additionalInfoListView);
            } catch (IOException e) {
                logger.error("Exception when trying to display an attack called " + newChosenAttack.toString());
            }
        }
    }

    private void onChosenSkillListViewItemSelection(ObservableValue<? extends DataObject> observable, DataObject oldSkill, DataObject newSkill) {
        //This null check prevents the call to DataGetter if selection of ListView was cleared and selected object became null
        if (newSkill != null) {
            try {
                clearSelectionOf(chosenUnitAttacksListView, chosenUnitEquipmentListView);
                List<String> skillDataList = new DataGetter(module).getDisplayableSkillDataOf(newSkill);
                putListIntoListView(skillDataList, additionalInfoListView);
            } catch (IOException e) {
                logger.error("Exception when trying to display a skill called " + newSkill.toString());
            }
        }
    }

    private void onChosenUnitEquipmentListViewItemSelection(ObservableValue<? extends DataObject> observable, DataObject oldEquipment, DataObject newEquipment) {
        //This null check prevents the call to DataGetter if selection of ListView was cleared and selected object became null
        if (newEquipment != null) {
            try {
                clearSelectionOf(chosenUnitSkillsListView, chosenUnitAttacksListView);
                List<String> equipmentDataList = new DataGetter(module).getDisplayableEquipmentData(newEquipment);
                putListIntoListView(equipmentDataList, additionalInfoListView);
            } catch (IOException e) {
                logger.error("Exception when trying to display an equipment called " + newEquipment.toString());
            }
        }
    }

    private void putListIntoListView(List list, ListView listView) {
        listView.getItems().clear();
        listView.getItems().addAll(list);
    }

    private void clearSelectionOf(ListView... listViews) {
        for (ListView listView : listViews) {
            listView.getSelectionModel().clearSelection();
        }
    }

    public void setModule(Module module) {
        this.module = module;
    }

    public Module getModule() {
        return module;
    }

}
