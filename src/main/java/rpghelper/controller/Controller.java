package rpghelper.controller;

import rpghelper.model.Module;

public interface Controller {
    /**
     * Allows to initialize elements of a controller which require initialized fields that don't get initialized
     * till after constructor and JavaFX initialize call.
     * Example of this is anything requiring a Module.
     *
     * @link{Controller{@link #setModule(Module)}}
     */
    void lateInitialize();

    void setModule(Module module);

    Module getModule();
}
