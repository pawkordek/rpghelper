package rpghelper;

import javafx.application.Application;
import javafx.stage.Stage;
import rpghelper.controller.ViewChanger;
import rpghelper.model.Logger;

public class RpgHelper extends Application {
    private static final org.apache.log4j.Logger logger = Logger.getInstance();
    public static final String DEFAULT_VIEW_FILES_RESOURCE_FOLDER_PATH = "/rpghelper/view/";

    public static void main(String[] args) {
        Logger.setup();
        logger.info("Starting the application");
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        ViewChanger viewChanger = new ViewChanger(stage, DEFAULT_VIEW_FILES_RESOURCE_FOLDER_PATH);
        viewChanger.changeViewTo("ModuleChooser");
    }
}
