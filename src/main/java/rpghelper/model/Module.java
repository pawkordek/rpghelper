package rpghelper.model;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

import java.io.IOException;
import java.util.*;

import static com.google.common.base.Preconditions.checkNotNull;

public class Module {
    private static final org.apache.log4j.Logger logger = Logger.getInstance();

    private final String NAME;
    private final String FOLDER_PATH;

    private final Map<String, DataObject> attacks;
    private final Map<String, DataObject> skills;
    private final Map<String, DataObject> equipment;
    private final Table<String, String, DataObject> units;
    private final static Module EMPTY_MODULE = new Module();

    private Module() {
        NAME = "\"Empty module\"";
        FOLDER_PATH = "\"Empty module folder location\"";
        attacks = new HashMap<>();
        skills = new HashMap<>();
        equipment = new HashMap<>();
        units = HashBasedTable.create();
    }

    private Module(String name,
                   String moduleFolderLocation,
                   String attacksJsonFilePath,
                   String skillsJsonFilePath,
                   String equipmentJsonFilePath,
                   String unitTypesJsonFilePath
    ) {
        NAME = checkNotNull(name, "Name of a module cannot be null");
        FOLDER_PATH = checkNotNull(moduleFolderLocation, "Module folder location cannot be null");
        Map<String, DataObject> tempAttacks = new HashMap<>();
        Map<String, DataObject> tempSkills = new HashMap<>();
        Map<String, DataObject> tempEquipment = new HashMap<>();
        Map<String, DataObject> tempUnitTypes;
        Table<String, String, DataObject> tempUnits = HashBasedTable.create();
        try {
            DataObjectLoader dataObjectLoader = new DataObjectLoader();
            tempAttacks = dataObjectLoader.getDataObjectsFrom(FOLDER_PATH + attacksJsonFilePath);
            tempSkills = dataObjectLoader.getDataObjectsFrom(FOLDER_PATH + skillsJsonFilePath);
            tempEquipment = dataObjectLoader.getDataObjectsFrom(FOLDER_PATH + equipmentJsonFilePath);
            tempUnitTypes = dataObjectLoader.getDataObjectsFrom(FOLDER_PATH + unitTypesJsonFilePath);
            for (Map.Entry<String, DataObject> unitType : tempUnitTypes.entrySet()) {
                Map<String, DataObject> units = dataObjectLoader.getDataObjectsFrom(FOLDER_PATH + unitType.getValue().getStringData("fileName"));
                for (DataObject unit : units.values()) {
                    tempUnits.put(unitType.getKey(), unit.getName(), unit);
                }
            }
        } catch (IOException e) {
            throw new IllegalStateException("Whole or part of the module data could not be loaded. True means the data is missing:\n" +
                    "Attacks: " + tempAttacks.isEmpty() + "\n" +
                    "Skills: " + tempSkills.isEmpty() + "\n" +
                    "Equipment: " + tempEquipment.isEmpty() + "\n" +
                    "Units: " + tempUnits.isEmpty() + "\n"
                    , e);
        }
        attacks = tempAttacks;
        skills = tempSkills;
        equipment = tempEquipment;
        units = tempUnits;
    }

    public static ModuleBuilder startBuilding() {
        return new ModuleBuilder();
    }

    public static class ModuleBuilder {
        private String name;
        private String moduleFolderLocation;
        private String attacksJsonFilePath;
        private String skillsJsonFilePath;
        private String equipmentJsonFilePath;
        private String unitTypesJsonFilePath;

        public ModuleBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public ModuleBuilder setModuleFolderPath(String moduleFolderPath) {
            this.moduleFolderLocation = moduleFolderPath;
            return this;
        }

        public ModuleBuilder setAttacksFilePath(String attacksJsonFilePath) {
            this.attacksJsonFilePath = attacksJsonFilePath;
            return this;
        }

        public ModuleBuilder setSkillsFilePath(String skillsJsonFilePath) {
            this.skillsJsonFilePath = skillsJsonFilePath;
            return this;
        }

        public ModuleBuilder setEquipmentFilePath(String equipmentJsonFilePath) {
            this.equipmentJsonFilePath = equipmentJsonFilePath;
            return this;
        }

        public ModuleBuilder setUnitTypesJsonFilePath(String unitTypesJsonFilePath) {
            this.unitTypesJsonFilePath = unitTypesJsonFilePath;
            return this;
        }

        public Module finishBuilding() {
            return new Module(
                    name,
                    moduleFolderLocation,
                    attacksJsonFilePath,
                    skillsJsonFilePath,
                    equipmentJsonFilePath,
                    unitTypesJsonFilePath
            );
        }
    }

    private boolean isDataEqualTo(Module module) {
        if (attacks.equals(module.attacks) &&
                skills.equals(module.skills) &&
                equipment.equals(module.equipment) &&
                units.equals(module.units)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (object == this) {
            return true;
        }
        if (!(object instanceof Module)) {
            return false;
        }
        Module module = (Module) object;
        if (!NAME.equals(module.getName())) {
            return false;
        }
        if (!FOLDER_PATH.equals(module.getFolderPath())) {
            return false;
        }
        if (!isDataEqualTo(module)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(NAME, FOLDER_PATH, attacks, skills, equipment, units);
    }

    public String getName() {
        return NAME;
    }

    @Override
    public String toString() {
        return getName();
    }

    public String getFolderPath() {
        return FOLDER_PATH;
    }

    public static Module getEmptyModule() {
        return EMPTY_MODULE;
    }

    public DataObject getAttack(String attackName) {
        return getDataObjectFromMap(attackName, attacks);
    }

    public DataObject getSkill(String skillName) {
        return getDataObjectFromMap(skillName, skills);
    }

    public DataObject getEquipment(String equipmentName) {
        return getDataObjectFromMap(equipmentName, equipment);
    }

    private DataObject getDataObjectFromMap(String objectName, Map<String, DataObject> map) {
        DataObject dataObject = map.get(objectName);
        if (dataObject == null) {
            logger.warn("Data object with a name=" + objectName + " has not been found.");
            return DataObject.getEmptyDataObject();
        } else {
            return dataObject.getCopy();
        }
    }

    public DataObject getUnit(String unitType, String unitName) {
        DataObject unit = units.get(unitType, unitName);
        if (unit == null) {
            logger.warn("Unit with a name=" + unitName + "and type " + unitType + " has not been found.");
            return DataObject.getEmptyDataObject();
        } else {
            return unit.getCopy();
        }
    }

    public List<String> getUnitTypesList() {
        return new ArrayList<>(units.rowKeySet());
    }

    public List<DataObject> getSkillsList() {
        return new ArrayList<>(skills.values());
    }

    public List<DataObject> getAttacksList() {
        return new ArrayList<>(attacks.values());
    }

    public List<DataObject> getEquipmentList() {
        return new ArrayList<>(equipment.values());
    }
}
