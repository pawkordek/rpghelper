package rpghelper.model;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

public class ModuleLoader {
    private static final org.apache.log4j.Logger logger = Logger.getInstance();

    private final String MODULES_FOLDER_PATH;

    public ModuleLoader(String modulesFolderPath) {
        MODULES_FOLDER_PATH = checkNotNull(modulesFolderPath, "Modules folder path cannot be null");
    }

    public List<String> getAvailablModules() throws IOException {
        List<String> availableModules = new ArrayList<>();
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get(MODULES_FOLDER_PATH))) {
            stream.forEach(path -> availableModules.add(getModuleNameFromPath(path.toString())));
        } catch (IOException e) {
            throw new IOException("Exception when loading list of available modules from location=" + MODULES_FOLDER_PATH, e);
        }
        return availableModules;
    }

    //Because path always looks like "module/MODULE_NAME" after splitting MODULE_NAME will be the first split element in the array
    private String getModuleNameFromPath(String path) {
        return path.split("/")[1];
    }

    public Module getModule(String name) {
        return Module.startBuilding()
                .setName(name)
                .setModuleFolderPath(MODULES_FOLDER_PATH + name + "/")
                .setAttacksFilePath("data/attacks/attacks.json")
                .setSkillsFilePath("data/skills/skills.json")
                .setEquipmentFilePath("data/equipment/equipment.json")
                .setUnitTypesJsonFilePath("data/units/types.json")
                .finishBuilding();
    }
}
