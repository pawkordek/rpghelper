package rpghelper.model;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

public class DataGetter {
    private final Module module;

    public DataGetter(Module module) {
        this.module = checkNotNull(module, "Module cannot be null");
    }

    public List<DataObject> getAttacksListOf(DataObject unit) {
        List<String> attacksNamesList = unit.getListData("attacks");
        List<DataObject> attacksList = new ArrayList<>();
        attacksNamesList.forEach(attackName -> attacksList.add(module.getAttack(attackName)));
        return attacksList;
    }

    public List<DataObject> getSkillsListOf(DataObject unit) {
        List<String> skillsNamesList = unit.getListData("skills");
        List<DataObject> skills = new ArrayList<>();
        skillsNamesList.forEach(skillName -> skills.add(module.getSkill(skillName)));
        return skills;
    }

    public List<DataObject> getEquipmentListOf(DataObject unit) {
        List<String> equipmentNamesList = unit.getListData("equipment");
        List<DataObject> equipment = new ArrayList<>();
        equipmentNamesList.forEach(equipmentName -> equipment.add(module.getEquipment(equipmentName)));
        return equipment;
    }

    public List<String> getStatisticsOf(DataObject unit) throws IOException {
        try {
            ScriptLauncher scriptLauncher = new ScriptLauncher(module.getFolderPath() + "scripts/getUnitDisplayData.lua");
            scriptLauncher.launchFunction("func", unit);
            List<String> unitDataList = scriptLauncher.returnStringList();
            return unitDataList;
        } catch (FileNotFoundException e) {
            throw new IOException("Script for loading unit statistics was not found", e);
        }
    }

    public List<String> getDisplayableAttackDataOf(DataObject attack) throws IOException {
        try {
            ScriptLauncher scriptLauncher = new ScriptLauncher(module.getFolderPath() + "scripts/getAttackDisplayData.lua");
            scriptLauncher.launchFunction("func", attack);
            return scriptLauncher.returnStringList();
        } catch (FileNotFoundException e) {
            throw new IOException("Script for loading attack data was not found", e);
        }
    }

    public List<String> getDisplayableEquipmentData(DataObject equipment) throws IOException {
        try {
            ScriptLauncher scriptLauncher = new ScriptLauncher(module.getFolderPath() + "scripts/getEquipmentDisplayData.lua");
            scriptLauncher.launchFunction("func", equipment);
            return scriptLauncher.returnStringList();
        } catch (FileNotFoundException e) {
            throw new IOException("Script for loading equipment data was not found", e);
        }
    }

    public List<String> getDisplayableSkillDataOf(DataObject skill) throws IOException {
        try {
            ScriptLauncher scriptLauncher = new ScriptLauncher(module.getFolderPath() + "scripts/getSkillDisplayData.lua");
            scriptLauncher.launchFunction("func", skill);
            return scriptLauncher.returnStringList();
        } catch (FileNotFoundException e) {
            throw new IOException("Script for loading skill data was not found", e);
        }
    }
}
