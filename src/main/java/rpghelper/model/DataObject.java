package rpghelper.model;

import java.util.*;

import static com.google.common.base.Preconditions.checkNotNull;

public class DataObject {
    private static final org.apache.log4j.Logger logger = Logger.getInstance();
    private final String name;
    private final Map<String, Object> data;

    public final static String NO_SUCH_DATA_STRING_ERROR = "No such data";
    public final static String UNSUPPORTED_DATA_TYPE_STRING_ERROR = "Unsupported data type";
    public final static List<String> NO_SUCH_DATA_LIST_ERROR = Collections.unmodifiableList(Arrays.asList("No such data"));
    public final static List<String> UNSUPPORTED_DATA_TYPE_LIST_ERROR = Collections.unmodifiableList(Arrays.asList("Unsupported data type"));
    public final static Map<String, Object> NO_SUCH_DATA_MAP_ERROR;
    public final static Map<String, Object> UNSUPPORTED_DATA_TYPE_MAP_ERROR;

    private final static DataObject EMPTY_DATA_OBJECT = new DataObject("\"Empty data object\"", new HashMap<>());

    static {
        Map<String, Object> tempMap = new HashMap<>();

        tempMap.put("No such data", "No such data");
        NO_SUCH_DATA_MAP_ERROR = Collections.unmodifiableMap(tempMap);

        tempMap.clear();
        tempMap.put("Unsupported data type", "Unsupported data type");
        UNSUPPORTED_DATA_TYPE_MAP_ERROR = Collections.unmodifiableMap(tempMap);
    }

    public DataObject(String name, Map<String, Object> data) {
        this.name = checkNotNull(name, "DataObject's name cannot be null");
        checkNotNull(data, "DataObject's data cannot be null");
        this.data = new HashMap<>(data);
    }

    public String getStringData(String identifier) {
        Object dataObject = data.get(identifier);
        if (dataObject == null) {
            return NO_SUCH_DATA_STRING_ERROR;
        } else {
            if (dataObject instanceof String) {
                return (String) dataObject;
            } else if (dataObject instanceof List) {
                return asString((List) dataObject);
            } else if (dataObject instanceof Map) {
                return asString((Map) dataObject);
            } else {
                logger.warn("Unsupported data type");
                return UNSUPPORTED_DATA_TYPE_STRING_ERROR;
            }
        }
    }

    private String asString(List list) {
        logger.warn("Returning " + list.getClass() + " as String");
        StringBuilder builder = new StringBuilder();
        list.forEach(element -> builder.append(element + ","));
        builder.deleteCharAt(builder.length() - 1);
        return builder.toString();
    }

    private String asString(Map map) {
        logger.warn("Returning " + map.getClass() + " as String");
        StringBuilder builder = new StringBuilder();
        map.forEach((key, value) -> builder.append("(" + key + "," + value + "),"));
        builder.deleteCharAt(builder.length() - 1);
        return builder.toString();
    }


    public List<String> getListData(String identifier) {
        Object dataObject = data.get(identifier);
        if (dataObject == null) {
            return NO_SUCH_DATA_LIST_ERROR;
        } else {
            if (dataObject instanceof List) {
                return (List) dataObject;
            } else if (dataObject instanceof String) {
                return asStringList((String) dataObject);
            } else if (dataObject instanceof Map) {
                return asStringList((Map) dataObject);
            } else {
                logger.warn("Unsupported data type");
                return UNSUPPORTED_DATA_TYPE_LIST_ERROR;
            }
        }
    }

    private List<String> asStringList(String text) {
        logger.warn("Returning " + text.getClass() + " as List");
        return new ArrayList<>(Arrays.asList((String) text));
    }

    private List<String> asStringList(Map map) {
        logger.warn("Returning " + map.getClass() + " as List");
        List<String> list = new ArrayList<>();
        map.forEach((key, value) -> list.add("(" + key + "," + value + ")"));
        return list;
    }

    public Map<String, Object> getMapData(String key) {
        Object dataObject = data.get(key);
        if (dataObject == null) {
            return NO_SUCH_DATA_MAP_ERROR;
        } else {
            if (dataObject instanceof Map) {
                return (Map) dataObject;
            } else if (dataObject instanceof String) {
                return asStringObjectMap(key, (String) dataObject);
            } else if (dataObject instanceof List) {
                return asStringObjectMap(key, (List) dataObject);
            } else {
                logger.warn("Unsupported data type");
                return UNSUPPORTED_DATA_TYPE_MAP_ERROR;
            }
        }
    }

    private Map<String, Object> asStringObjectMap(String key, String text) {
        logger.warn("Returning " + text.getClass() + " as Map");
        Map<String, Object> map = new HashMap<>();
        map.put(key, text);
        return map;
    }

    private Map<String, Object> asStringObjectMap(String key, List list) {
        logger.warn("Returning " + list.getClass() + " as Map");
        Map<String, Object> map = new HashMap<>();
        for (int i = 0; i <= list.size() - 1; ++i) {
            map.put(key + i, list.get(i));
        }
        return map;
    }

    public DataObject getCopy() {
        return new DataObject(name, data);
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (object == this) {
            return true;
        }
        if (!(object instanceof DataObject)) {
            return false;
        }
        DataObject dataObject = (DataObject) object;
        if (!name.equals(dataObject.name)) {
            return false;
        }
        if (!data.equals(dataObject.data)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, data);
    }

    public static DataObject getEmptyDataObject() {
        return EMPTY_DATA_OBJECT;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
