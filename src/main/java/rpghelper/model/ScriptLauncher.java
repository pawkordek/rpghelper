package rpghelper.model;

import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaError;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.jse.CoerceJavaToLua;
import org.luaj.vm2.lib.jse.JsePlatform;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

//TODO: Prevent calling of non existing functions
//TODO: Prevent returning null values out of return functions when script is, for example, broken or incomplete
public class ScriptLauncher {
    private static final org.apache.log4j.Logger logger = Logger.getInstance();

    private Globals globals = JsePlatform.standardGlobals();
    private LuaValue lastReturnValue;
    private LuaValue script;

    public ScriptLauncher(String scriptFilePath) throws FileNotFoundException {
        checkNotNull(scriptFilePath, "Script file path cannot be null");
        try {
            script = globals.loadfile(scriptFilePath);
        } catch (LuaError e) {
            throw new FileNotFoundException("Requested script in path: " + scriptFilePath + " does not exist.");
        }
    }

    public void launchScript() {
        lastReturnValue = script.call();
    }

    public void launchFunction(String name) {
        checkNotNull(name, "Function name cannot be null");
        lastReturnValue = getFunction(name).call();
    }

    public void launchFunction(String name, Object parameter) {
        checkNotNull(name, "Function name cannot be null");
        checkNotNull(parameter, "Function parameter cannot be null");
        LuaValue luaArgument = CoerceJavaToLua.coerce(parameter);
        lastReturnValue = getFunction(name).call(luaArgument);
    }

    private LuaValue getFunction(String name) {
        script.call();
        return globals.get(name);
    }

    public String returnString() {
        return lastReturnValue.toString();
    }

    public List<String> returnStringList() {
        int tabLenght = lastReturnValue.length();
        List<String> list = new ArrayList<>();
        for (int i = 1; i <= tabLenght; ++i) {
            list.add(lastReturnValue.get(i).toString());
        }
        return list;
    }

}
