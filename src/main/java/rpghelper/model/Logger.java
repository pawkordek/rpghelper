package rpghelper.model;

import java.io.IOException;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.PatternLayout;

public class Logger {
	private final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(Logger.class);

	private final static String LOGGING_PATTERN = "%r [%t] %p %l %x - %m%n";

	public static void setup() {
		PatternLayout patternLayout = new PatternLayout(LOGGING_PATTERN);
		logger.addAppender(new ConsoleAppender(patternLayout));
		try {
			FileAppender appender = new FileAppender(patternLayout, "log.txt", false);
			logger.addAppender(appender);
			appender.setName("File Appender");
		} catch (IOException e) {
			logger.error("Exception when setting up logger", e);
		}
	}

	public static org.apache.log4j.Logger getInstance() {
		return logger;
	}
}
