package rpghelper.model;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

public class DataObjectLoader {
    private static final org.apache.log4j.Logger logger = Logger.getInstance();

    public Map<String, DataObject> getDataObjectsFrom(String jsonFilePath) throws IOException {
        try {
            checkNotNull(jsonFilePath, "Path of a JSON file is null");
            Map<String, DataObject> loadedDataObjects = new HashMap<>();
            File jsonFile = new File(jsonFilePath);
            ObjectMapper objectMapper = new ObjectMapper();
            Map<String, Map<String, Object>> dataObjectsMap = objectMapper.readValue(jsonFile, Map.class);
            dataObjectsMap.forEach((objectName, objectData) ->
                    loadedDataObjects.put(objectName, new DataObject(objectName, objectData)));
            return loadedDataObjects;
        } catch (IOException e) {
            throw new IOException("Error when loading units in path=" + jsonFilePath, e);
        }
    }
}
