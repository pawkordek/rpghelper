package rpghelper.model;

import org.junit.Before;
import org.junit.Test;
import rpghelper.helper.TestDataProvider;
import rpghelper.helper.TestModuleProvider;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ModuleTest {
    private Module moduleOne;
    private Module moduleTwo;
    private Module moduleDifferent;

    private TestModuleProvider testModuleProvider = new TestModuleProvider();
    private TestDataProvider testDataProvider = new TestDataProvider();

    @Before
    public void setup() {
        moduleOne = testModuleProvider.getTestModule();
        moduleTwo = testModuleProvider.getTestModule();
        moduleDifferent = testModuleProvider.getTestCopyModule();
    }

    @Test
    public void Module_shouldThrowAnException_ifSomeDataCannotBeLoadedOnItsConstruction() {
        assertThrows(IllegalStateException.class, () -> Module.startBuilding().setName("").setModuleFolderPath("").finishBuilding());
    }

    @Test
    public void TwoModules_withTheSameData_shouldBeEqual() throws Exception {
        assertEquals(moduleOne, moduleTwo);
    }

    @Test
    public void TwoModules_withDifferentData_shouldNotBeEqual() throws Exception {
        assertNotEquals(moduleOne, moduleDifferent);
    }

    @Test
    public void TwoModules_withTheSameData_shouldHaveTheSameHashCode() throws Exception {
        int hashOne = moduleOne.hashCode();
        int hashTwo = moduleTwo.hashCode();
        assertEquals(hashOne, hashTwo);
    }

    @Test
    public void TwoModules_withDifferentData_shouldNotHaveTheSameHashCode() throws Exception {
        int hashOne = moduleOne.hashCode();
        int hashTwo = moduleDifferent.hashCode();
        assertNotEquals(hashOne, hashTwo);
    }

    @Test
    public void Module_shouldReturnTheSameAttack_asProvided() throws Exception {
        DataObject attack = testDataProvider.getAttack("Kick");
        assertEquals(attack, moduleOne.getAttack("Kick"));
    }

    @Test
    public void Module_shouldReturnTheSameSkill_asProvided() throws Exception {
        DataObject skill = testDataProvider.getSkill("Fireball");
        assertEquals(skill, moduleOne.getSkill("Fireball"));
    }

    @Test
    public void Module_shouldReturnTheSameEquipment_asProvided() throws Exception {
        DataObject equipment = testDataProvider.getEquipment("Old wand");
        assertEquals(equipment, moduleOne.getEquipment("Old wand"));
    }

    @Test
    public void Module_shouldReturnTheSameUnit_asProvided() throws Exception {
        Map<String, Object> unitData = new HashMap<>();
        unitData.put("text", "I'm a simple unit to test.");
        DataObject unit = new DataObject("Simple", unitData);
        assertEquals(unit, moduleOne.getUnit("test", "Simple"));
    }

    @Test
    public void Module_shouldReturnUnitTypes_thatAgreeWithProvidedOnes() throws Exception {
        String type1 = "test";
        String type2 = "empty";
        List<String> unitTypesList = moduleOne.getUnitTypesList();
        assertTrue(unitTypesList.contains(type1));
        assertTrue(unitTypesList.contains(type2));
    }

    @Test
    public void Module_shouldReturnAttacks_thatAgreeWithProvidedOnes() throws Exception {
        DataObject attack = testDataProvider.getAttack("Kick");
        List<DataObject> attacks = moduleOne.getAttacksList();
        assertTrue(attacks.contains(attack));
    }

    @Test
    public void Module_shouldReturnSkills_thatAgreeWithProvidedOnes() throws Exception {
        DataObject skill = testDataProvider.getSkill("Fireball");
        List<DataObject> skills = moduleOne.getSkillsList();
        assertTrue(skills.contains(skill));
    }

    @Test
    public void Module_shouldReturnEquipment_thatAgreeWithProvidedOnes() throws Exception {
        DataObject equipment = testDataProvider.getEquipment("Old wand");
        List<DataObject> equipmentList = moduleOne.getEquipmentList();
        assertTrue(equipmentList.contains(equipment));
    }
}
