package rpghelper.model;

import org.junit.Before;
import org.junit.Test;
import rpghelper.helper.TestDataProvider;
import rpghelper.helper.TestModuleProvider;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

//TODO: Make sure all tests names have the same standard of naming
public class DataGetterTest {
    private DataObject unitDataObject;
    private DataGetter dataGetter;
    private final TestDataProvider testDataProvider = new TestDataProvider();

    Map<String, Object> unitData = new HashMap<>();
    List<String> unitAttacks = new ArrayList<>();
    List<String> unitSkills = new ArrayList<>();
    List<String> unitEquipment = new ArrayList<>();

    @Before
    public void setup() {
        unitAttacks.add("Kick");
        unitData.put("attacks", unitAttacks);

        unitSkills.add("Fireball");
        unitSkills.add("Heal");
        unitData.put("skills", unitSkills);

        unitEquipment.add("Old wand");
        unitData.put("equipment", unitEquipment);

        unitData.put("image", "test.png");

        unitDataObject = new DataObject("TestUnit", unitData);

        dataGetter = new DataGetter(new TestModuleProvider().getTestModule());
    }

    @Test
    public void ShouldReturnUnitAttacks_thatAreTheSame_asProvided() throws Exception {
        assertTrue(dataGetter.getAttacksListOf(unitDataObject).contains(testDataProvider.getAttack("Kick")));
    }

    @Test
    public void ShouldReturnUnitSkills_thatAreTheSame_asProvided() throws Exception {
        assertTrue(dataGetter.getSkillsListOf(unitDataObject).contains(testDataProvider.getSkill("Fireball")));
        assertTrue(dataGetter.getSkillsListOf(unitDataObject).contains(testDataProvider.getSkill("Heal")));
    }

    @Test
    public void ShouldReturnUnitEquipment_thatAreTheSame_asProvided() throws Exception {
        assertTrue(dataGetter.getEquipmentListOf(unitDataObject).contains(testDataProvider.getEquipment("Old wand")));
    }

    @Test
    public void ShouldReturn_unit_statistics() throws Exception {
        List<String> correctData = new ArrayList<>(Arrays.asList("name TestUnit", "image test.png"));
        assertEquals(correctData, dataGetter.getStatisticsOf(unitDataObject));
    }

    @Test
    public void ShouldReturn_attack_data() throws Exception {
        List<String> correctData = new ArrayList<>(Arrays.asList("Description: A normal kick."));
        assertEquals(correctData, dataGetter.getDisplayableAttackDataOf(testDataProvider.getAttack("Kick")));
    }

    @Test
    public void ShouldReturn_equpiment_data() throws Exception {
        List<String> correctData = new ArrayList<>(Arrays.asList("Description: An old wand from ancient times."));
        assertEquals(correctData, dataGetter.getDisplayableEquipmentData(testDataProvider.getEquipment("Old wand")));
    }

    @Test
    public void ShouldReturn_skills_data() throws Exception {
        List<String> correctData = new ArrayList<>(Arrays.asList("Description: A healing energy that restores 10 hp to an ally."));
        assertEquals(correctData, dataGetter.getDisplayableSkillDataOf(testDataProvider.getSkill("Heal")));
    }
}
