package rpghelper.model;

import org.junit.Before;
import org.junit.Test;
import rpghelper.helper.TestModuleProvider;

import java.io.IOException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class ModuleLoaderTest {

    private ModuleLoader moduleLoader;

    @Before
    public void setup() throws Exception {
        moduleLoader = new ModuleLoader("modules/");
    }

    @Test
    public void ReturnedListOfAvailableModules_shouldBeEmpty_ifFolderWithModulesIsEmpty() throws Exception {
        ModuleLoader moduleLoaderWithWrongFolderPath = new ModuleLoader("modules/emptyFolder");
        assertTrue(moduleLoaderWithWrongFolderPath.getAvailablModules().isEmpty());
    }

    @Test
    public void ShouldThrowAnException_ifGivenModulesFolderPathIsInvalid_whenTryingToGetAvaialableModules() throws Exception {
        assertThrows(IOException.class, () -> new ModuleLoader("invalid path").getAvailablModules());
    }

    @Test
    public void ReturnedList_shouldNotBeEmpty_ifModulesFolderPathIsCorrectAndThereAreModulesFolders() throws Exception {
        assertFalse(moduleLoader.getAvailablModules().isEmpty());
    }

    @Test
    public void ReturnedModule_shouldBeEqual_toTheOneProvided() throws Exception {
        Module module = new TestModuleProvider().getTestModule();
        assertEquals(module, moduleLoader.getModule("test"));
    }

}
