package rpghelper.model;

import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class DataObjectTest {
    //FIXME: Move all tests objects initializations in all tests into @Before
    private Map<String, Object> mockData = new HashMap<>();
    private DataObject dataObject;

    @Before
    public void setup() throws Exception {
        mockData.put("text", "Some text");
        List<String> numbersList = new ArrayList<>(Arrays.asList("1", "2", "3"));
        mockData.put("numbers", new ArrayList<>(Arrays.asList("1", "2", "3")));
        Map<String, String> lettersMap = new HashMap<>();
        lettersMap.put("a", "A");
        lettersMap.put("b", "B");
        lettersMap.put("c", "C");
        mockData.put("letters", lettersMap);
        Set<String> wrongData = new HashSet<>();
        wrongData.add("one");
        wrongData.add("two");
        wrongData.add("three");
        mockData.put("wrongData", wrongData);
        dataObject = new DataObject("TestUnit", mockData);
    }

    @Test
    public void DataObject_shouldReturnDataThasIsAStringAsString_whenAskedForIt() throws Exception {
        String data = this.dataObject.getStringData("text");
        assertEquals("Some text", data);
    }

    @Test
    public void DataObject_shouldReturnDataThatIsAListAsString_whenAskedForIt() throws Exception {
        String data = this.dataObject.getStringData("numbers");
        assertEquals("1,2,3", data);
    }

    @Test
    public void DataObject_shouldReturnDataThatIsAMapAsString_whenAskedForIt() throws Exception {
        String data = this.dataObject.getStringData("letters");
        assertEquals("(a,A),(b,B),(c,C)", data);
    }

    @Test
    public void DataObject_shouldReturnErrorMessageString_whenReturningStringDataThatIsUnsupported() throws Exception {
        String error = dataObject.getStringData("wrongData");
        assertEquals(DataObject.UNSUPPORTED_DATA_TYPE_STRING_ERROR, error);
    }

    @Test
    public void DataObject_shouldReturnErrorMessageString_whenReturningStringDataThatDoesntExist() throws Exception {
        String error = dataObject.getStringData("This data definitively doesn't exist");
        assertEquals(DataObject.NO_SUCH_DATA_STRING_ERROR, error);
    }

    @Test
    public void DataObject_shouldReturnDataThasIsAListAsList_whenAskedForIt() throws Exception {
        List<String> data = this.dataObject.getListData("numbers");
        List<String> correctData = new ArrayList<>(Arrays.asList("1", "2", "3"));
        assertEquals(correctData, data);
    }

    @Test
    public void DataObject_shouldReturnDataThasIsAStringAsList_whenAskedForIt() throws Exception {
        List<String> data = this.dataObject.getListData("text");
        List<String> correctData = new ArrayList<>(Arrays.asList("Some text"));
        assertEquals(correctData, data);
    }

    @Test
    public void DataObject_shouldReturnDataThasIsAMapAsList_whenAskedForIt() throws Exception {
        List<String> data = this.dataObject.getListData("letters");
        List<String> correctData = new ArrayList<>(Arrays.asList("(a,A)", "(b,B)", "(c,C)"));
        assertEquals(correctData, data);
    }

    @Test
    public void DataObject_shouldReturnErrorMessageList_whenReturningListDataThatIsUnsupported() throws Exception {
        List<String> error = dataObject.getListData("wrongData");
        assertEquals(DataObject.UNSUPPORTED_DATA_TYPE_LIST_ERROR, error);
    }

    @Test
    public void DataObject_shouldReturnErrorMessageList_whenReturningListDataThatDoesntExist() throws Exception {
        List<String> error = dataObject.getListData("This data definitively doesn't exist");
        assertEquals(DataObject.NO_SUCH_DATA_LIST_ERROR, error);
    }

    @Test
    public void DataObject_shouldReturnDataThasIsAStringAsMap_whenAskedForIt() throws Exception {
        Map<String, Object> data = this.dataObject.getMapData("text");
        Map<String, Object> correctData = new HashMap<>();
        correctData.put("text", "Some text");
        assertEquals(correctData, data);
    }

    @Test
    public void DataObject_shouldReturnDataThasIsAListAsMap_whenAskedForIt() throws Exception {
        Map<String, Object> data = this.dataObject.getMapData("numbers");
        Map<String, Object> correctData = new HashMap<>();
        correctData.put("numbers0", "1");
        correctData.put("numbers1", "2");
        correctData.put("numbers2", "3");
        assertEquals(correctData, data);
    }

    @Test
    public void DataObject_shouldReturnDataThasIsAMapAsMap_whenAskedForIt() throws Exception {
        Map<String, Object> data = this.dataObject.getMapData("letters");
        Map<String, Object> correctData = new HashMap<>();
        correctData.put("a", "A");
        correctData.put("b", "B");
        correctData.put("c", "C");
        assertEquals(correctData, data);
    }

    @Test
    public void DataObject_shouldReturnErrorMessageMap_whenReturningMapDataThatIsUnsupported() throws Exception {
        Map<String, Object> error = dataObject.getMapData("wrongData");
        assertEquals(DataObject.UNSUPPORTED_DATA_TYPE_MAP_ERROR, error);
    }

    @Test
    public void DataObject_shouldReturnErrorMessageMap_whenReturningMapDataThatDoesntExist() throws Exception {
        Map<String, Object> error = dataObject.getMapData("This data definitively doesn't exist");
        assertEquals(DataObject.NO_SUCH_DATA_MAP_ERROR, error);
    }

    @Test
    public void DataObject_shouldReturnACopy_ThatIsEqualToIt() throws Exception {
        DataObject dataObjectCopy = dataObject.getCopy();
        assertEquals(dataObject, dataObjectCopy);
    }

    @Test
    public void DataObject_shouldNotBeEqual_ToADifferentDataObject() throws Exception {
        DataObject emptyDataObject = new DataObject("", new HashMap<>());
        assertNotEquals(dataObject, emptyDataObject);
    }

    @Test
    public void DataObject_shouldBeEqual_toDataObjectWithTheSameNameAndData() throws Exception {
        Map<String, Object> firstData = new HashMap<>();
        firstData.put("key", "value");
        DataObject firstDataObject = new DataObject("name", firstData);
        Map<String, Object> secondData = new HashMap<>();
        secondData.put("key", "value");
        DataObject secondDataObject = new DataObject("name", secondData);
        assertEquals(firstDataObject, secondDataObject);
    }

    @Test
    public void DataObject_shouldReturnACopy_ThatHasTheSameHashCode() throws Exception {
        int hashCodeOfDataCopy = dataObject.getCopy().hashCode();
        int hashCodeOfOriginalData = dataObject.hashCode();
        assertEquals(hashCodeOfOriginalData, hashCodeOfDataCopy);
    }

    @Test
    public void DataObjectHashCode_shouldNotBeEqual_ToADifferentDataObjectsHashCode() throws Exception {
        int hashCodeOfOriginalData = dataObject.hashCode();
        int hashCodeOfDifferentData = new DataObject("", new HashMap<>()).hashCode();
        assertNotEquals(hashCodeOfOriginalData, hashCodeOfDifferentData);
    }
}

