package rpghelper.model;

import org.junit.Before;
import org.junit.Test;
import rpghelper.helper.TestModuleProvider;

import java.io.IOException;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class DataObjectLoaderTest {

    Module mockModule;
    DataObjectLoader dataObjectLoader = new DataObjectLoader();

    @Before
    public void setup() throws Exception {
        mockModule = new TestModuleProvider().getTestModule();
    }

    @Test
    public void DataObjectLoader_shouldThrowAnException_whenTryingToLoadDataThatDoesNotExist() throws Exception {
        assertThrows(IOException.class, () -> dataObjectLoader.getDataObjectsFrom("Nonexistent location"));
    }

    @Test
    public void ReturnedDataObjectsMap_shouldNotBeEmpty_ifDataExists() throws Exception {
        assertNotNull(dataObjectLoader.getDataObjectsFrom("modules/test/data/sampleData.json"));
    }

    @Test
    public void ReturnedData_shouldBeDifferent_ifDifferentDataWasLoaded() throws Exception {
        assertNotEquals(dataObjectLoader.getDataObjectsFrom("modules/test/data/sampleData.json"),
                dataObjectLoader.getDataObjectsFrom("modules/test/data/sampleData2.json"));
    }
}

