package rpghelper.model;

import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ScriptLauncherTest {
    private Module mockModule = mock(Module.class);

    //FIXME: Don't mock the module
    @Before
    public void setup() {
        when(mockModule.getName()).thenReturn("test");
        when(mockModule.getFolderPath()).thenReturn("modules/test/");
    }

    @Test
    public void RunningAScript_shouldReturn_HelloWorld() throws Exception {
        ScriptLauncher scriptLauncher = new ScriptLauncher(mockModule.getFolderPath() + "scripts/returnHelloWorld.lua");
        scriptLauncher.launchScript();
        assertEquals("Hello world", scriptLauncher.returnString());
    }

    @Test
    public void TryingToLoadANonExistingScript_shouldThrow_AFileNotFoundException() throws Exception {
        assertThrows(FileNotFoundException.class, () -> new ScriptLauncher("There is no way this script exists.lua"));
    }

    @Test
    public void RunningAFunction_shouldReturn_AString() throws Exception {
        ScriptLauncher scriptLauncher = new ScriptLauncher(mockModule.getFolderPath() + "scripts/helloFromFunction.lua");
        scriptLauncher.launchFunction("hello_from_function");
        assertEquals("Hello from function", scriptLauncher.returnString());
    }

    @Test
    public void RunningAScript_shouldReturn_StringList() throws Exception {
        ScriptLauncher scriptLauncher = new ScriptLauncher(mockModule.getFolderPath() + "scripts/returnStringArray.lua");
        scriptLauncher.launchScript();
        List<String> correctStrings = new ArrayList<>(Arrays.asList("one", "two", "three"));
        assertEquals(correctStrings, scriptLauncher.returnStringList());
    }

    @Test
    public void RunningAFunctionWithArgument_shouldReturnThisParameter_AsString() throws Exception {
        ScriptLauncher scriptLauncher = new ScriptLauncher(mockModule.getFolderPath() + "scripts/oneParameterFunction.lua");
        scriptLauncher.launchFunction("hello_test", "Hello test");
        assertEquals("Hello test", scriptLauncher.returnString());
    }
}
