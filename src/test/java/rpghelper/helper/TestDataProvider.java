package rpghelper.helper;

import rpghelper.model.DataObject;

import java.util.HashMap;
import java.util.Map;

public class TestDataProvider {

    public DataObject getSkill(String name) {
        if (name.equals("Fireball")) {
            Map<String, Object> fireballData = new HashMap<>();
            fireballData.put("description", "A big flaming orb thrown into enemies. Does 10 dmg.");
            return new DataObject("Fireball", fireballData);
        } else if (name.equals("Heal")) {
            Map<String, Object> healData = new HashMap<>();
            healData.put("description", "A healing energy that restores 10 hp to an ally.");
            return new DataObject("Heal", healData);
        } else {
            return DataObject.getEmptyDataObject();
        }
    }

    public DataObject getEquipment(String name) {
        if (name.equals("Old wand")) {
            Map<String, Object> equipmentData = new HashMap<>();
            equipmentData.put("description", "An old wand from ancient times.");
            return new DataObject("Old wand", equipmentData);
        } else {
            return DataObject.getEmptyDataObject();
        }
    }

    public DataObject getAttack(String name) {
        if (name.equals("Kick")) {
            Map<String, Object> kickData = new HashMap<>();
            kickData.put("description", "A normal kick.");
            return new DataObject("Kick", kickData);
        } else {
            return DataObject.getEmptyDataObject();
        }
    }
}
