package rpghelper.helper;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.testfx.api.FxRobot;
import org.testfx.framework.junit.ApplicationTest;
import rpghelper.RpgHelper;
import rpghelper.controller.Controller;

public class FxApplicationTest extends ApplicationTest {
    private final String VIEWS_LOCATION = "/rpghelper/view/";
    private final String VIEW_FILE_EXTENSION = ".fxml";

    private String viewName;

    protected Parent mainNode;
    protected Stage stage;
    protected final FxRobot robot = new FxRobot();
    protected Controller currentController;

    protected <T extends Node> T getGuiElement(String elementId) {
        return lookup(elementId).query();
    }

    final public void setViewName(String newName) {
        viewName = newName;
    }

    /**
     * Should be overridden and used to set up things in the class that are requried before FxApplication creation.
     * Example of such required element is viewName variable.
     * This function is run before @link{FxApplicationTest{@link #start(Stage)}} method.
     *
     * @throws Exception an exception depending on method implementation
     */
    @Override
    public void init() throws Exception {
    }

    @Override
    final public void start(Stage stage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(RpgHelper.class.getResource(VIEWS_LOCATION + viewName + VIEW_FILE_EXTENSION));
        mainNode = fxmlLoader.load();
        currentController = fxmlLoader.getController();
        stage.setScene(new Scene(mainNode));
        stage.show();
        stage.toFront();
        this.stage = stage;
    }
}
