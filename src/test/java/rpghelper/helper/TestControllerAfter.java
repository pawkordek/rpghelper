package rpghelper.helper;

import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import rpghelper.controller.Controller;
import rpghelper.model.Module;

public class TestControllerAfter implements Controller {
    private Module module;

    @FXML
    private ListView<Module> moduleTestListView;

    @Override
    public void lateInitialize() {
        moduleTestListView.getItems().add(module);
    }

    @Override
    public void setModule(Module module) {
        this.module = module;
    }

    @Override
    public Module getModule() {
        return module;
    }
}
