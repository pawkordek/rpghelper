package rpghelper.helper;

import rpghelper.model.Module;

public class TestModuleProvider {
    private final Module testModule = Module.startBuilding()
            .setName("test")
            .setModuleFolderPath("modules/test/")
            .setAttacksFilePath("data/attacks/attacks.json")
            .setSkillsFilePath("data/skills/skills.json")
            .setEquipmentFilePath("data/equipment/equipment.json")
            .setUnitTypesJsonFilePath("data/units/types.json")
            .finishBuilding();
    private final Module testCopyModule = Module.startBuilding()
            .setName("test")
            .setModuleFolderPath("modules/testCopy/")
            .setAttacksFilePath("data/attacks/attacks.json")
            .setSkillsFilePath("data/skills/skills.json")
            .setEquipmentFilePath("data/equipment/equipment.json")
            .setUnitTypesJsonFilePath("data/units/types.json")
            .finishBuilding();

    public Module getTestModule() {
        return testModule;
    }

    public Module getTestCopyModule() {
        return testCopyModule;
    }
}
