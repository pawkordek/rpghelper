package rpghelper.helper;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import rpghelper.controller.Controller;
import rpghelper.model.Module;

public class TestController implements Controller {

    private Module module = new TestModuleProvider().getTestModule();
    @FXML
    Button testButton;

    @Override
    public void lateInitialize() {

    }

    @Override
    public void setModule(Module module) {

    }

    @Override
    public Module getModule() {
        return module;
    }

}
