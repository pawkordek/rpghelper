package rpghelper.controller;

import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import org.junit.Before;
import org.junit.Test;
import org.testfx.util.WaitForAsyncUtils;
import rpghelper.helper.FxApplicationTest;
import rpghelper.helper.TestModuleProvider;
import rpghelper.model.Module;

import java.util.List;

import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ModuleChooserControllerTest extends FxApplicationTest {
    Module testModule;

    Button confirmModuleButton;
    ChoiceBox<Module> moduleChooserChoiceBox;

    @Override
    public void init() throws Exception {
        setViewName("ModuleChooser");
    }

    @Before
    public void setup() throws Exception {
        confirmModuleButton = getGuiElement("#confirmModuleButton");
        moduleChooserChoiceBox = getGuiElement("#moduleChooserChoiceBox");

        testModule = new TestModuleProvider().getTestModule();

        currentController.setModule(testModule);
        currentController.lateInitialize();
    }

    @Test
    public void ConfirmModuleButton_shouldChangeViewToBattlePreparation_whenClicked() throws Exception {
        Platform.runLater(() -> moduleChooserChoiceBox.getSelectionModel().select(2));
        robot.clickOn(confirmModuleButton);
        WaitForAsyncUtils.waitForFxEvents();
        TextField textField = getGuiElement("#unitChooserTextField");
        assertNotNull(textField);
    }

    @Test
    public void ModuleChooserChoiceBox_shouldHaveModulesNames_onControllerStart() throws Exception {
        List<Module> list = moduleChooserChoiceBox.getItems();
        assertNotEquals(0, list.size());
    }

    @Test
    public void FirstModuleInModuleChooserChoiceBox_shouldBeChosen_onControllerStart() throws Exception {
        WaitForAsyncUtils.waitForFxEvents();
        assertNotNull(moduleChooserChoiceBox.getSelectionModel().getSelectedItem());
    }

    //TODO: Test somehow the alert when there are no modules available

}
