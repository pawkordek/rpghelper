package rpghelper.controller;

import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import org.junit.Before;
import org.junit.Test;
import org.testfx.api.FxRobotException;
import org.testfx.util.WaitForAsyncUtils;
import rpghelper.helper.FxApplicationTest;
import rpghelper.helper.TestDataProvider;
import rpghelper.helper.TestModuleProvider;
import rpghelper.model.DataObject;
import rpghelper.model.Module;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class BattlePreparationControllerTest extends FxApplicationTest {
    private ListView<DataObject> unitsInBattleListView;
    private Button addUnitToBattleButton;
    private TextField unitChooserTextField;
    private ImageView unitImageView;
    private ListView<String> chosenUnitStatsListView;
    private ListView<DataObject> chosenUnitSkillsListView;
    private ListView<DataObject> chosenUnitEquipmentListView;
    private ListView<DataObject> chosenUnitAttacksListView;
    private ListView<String> additionalInfoListView;
    private ChoiceBox unitTypesChoiceBox;

    private Module module;

    private TestDataProvider testDataProvider;

    @Override
    public void init() throws Exception {
        setViewName("BattlePreparation");
    }

    @Before
    public void setup() throws Exception {
        addUnitToBattleButton = getGuiElement("#addUnitToBattleButton");
        unitsInBattleListView = getGuiElement("#unitsInBattleListView");
        unitChooserTextField = getGuiElement("#unitChooserTextField");
        unitImageView = getGuiElement("#unitImageView");
        chosenUnitStatsListView = getGuiElement("#chosenUnitStatsListView");
        chosenUnitSkillsListView = getGuiElement("#chosenUnitSkillsListView");
        chosenUnitEquipmentListView = getGuiElement("#chosenUnitEquipmentListView");
        chosenUnitAttacksListView = getGuiElement("#chosenUnitAttacksListView");
        additionalInfoListView = getGuiElement("#additionalInfoListView");
        unitTypesChoiceBox = getGuiElement("#unitTypesChoiceBox");

        testDataProvider = new TestDataProvider();
        module = new TestModuleProvider().getTestModule();

        currentController.setModule(module);
        currentController.lateInitialize();
    }

    @Test
    public void UnitsTypesChoiceBox_shouldNotBeEmpty_OnControllerStart() throws Exception {
        assertFalse(unitTypesChoiceBox.getItems().isEmpty());
    }

    @Test
    public void UnitTypeInUnitTypesChoiceBox_shouldBeSelected_onControllerStart() throws Exception {
        WaitForAsyncUtils.waitForFxEvents();
        assertNotNull(unitTypesChoiceBox.getSelectionModel().getSelectedItem());
    }

    private void addUnitToBattle(String unitName) {
        unitChooserTextField.setText(unitName);
        Platform.runLater(() -> unitTypesChoiceBox.getSelectionModel().selectFirst());
        robot.clickOn(addUnitToBattleButton);
    }

    @Test
    public void UnitsInBattleList_shouldBeEmptyIfUnitDoesNotExist_afterAddUnitsToBattleButtonWasPressed() throws Exception {
        addUnitToBattle("This unit does not exist");
        robot.clickOn("OK");
        assertEquals(true, unitsInBattleListView.getItems().isEmpty());
    }

    @Test
    public void Alert_shouldBeShown_ifUnitDoesNotExist() throws Exception {
        addUnitToBattle("This unit does not exist");
        try {
            robot.clickOn("OK");
            //The exception will be thrown if the robot cannot find the button to click on it
        } catch (FxRobotException e) {
            fail("The alert was not shown");
        }
    }

    private DataObject selectFirstUnit() {
        Platform.runLater(() -> unitsInBattleListView.getSelectionModel().selectFirst());
        return unitsInBattleListView.getSelectionModel().getSelectedItem();
    }

    @Test
    public void UnitsInBattleList_shouldContainElementWithNameTestUnit_afterAddUnitsToBattleButtonWasPressed() throws Exception {
        addUnitToBattle("TestUnit");
        DataObject selectedUnit = selectFirstUnit();
        WaitForAsyncUtils.waitForFxEvents();
        if (selectedUnit == null) {
            fail("unitsInBattleList is null");
        } else {
            assertEquals("TestUnit", selectedUnit.getName());
        }
    }

    @Test
    public void UnitsImage_shouldBeShown_afterItWasClickedOnTheList() throws Exception {
        addUnitToBattle("TestUnit");
        selectFirstUnit();
        WaitForAsyncUtils.waitForFxEvents();
        assertNotNull(unitImageView.getImage());
    }

    @Test
    public void ChosenUnitDataListView_shouldContainCertainUnitStatisticsData_afterUnitWasClickedOnTheList() throws Exception {
        addUnitToBattle("TestUnit");
        selectFirstUnit();
        WaitForAsyncUtils.waitForFxEvents();
        List<String> correctData = new ArrayList<>(Arrays.asList("name TestUnit", "image test.png"));
        assertEquals(correctData, chosenUnitStatsListView.getItems());
    }

    @Test
    public void ChosenUnitSkillsListView_shouldContainCertainUnitSkills_afterUnitWasClickedOnTheList() throws Exception {
        addUnitToBattle("TestUnit");
        selectFirstUnit();
        WaitForAsyncUtils.waitForFxEvents();

        DataObject fireball = testDataProvider.getSkill("Fireball");
        DataObject heal = testDataProvider.getSkill("Heal");

        assertTrue(chosenUnitSkillsListView.getItems().contains(fireball));
        assertTrue(chosenUnitSkillsListView.getItems().contains(heal));
    }

    @Test
    public void ChosenUnitEquipmentListView_shouldContainCertainUnitEquipment_afterUnitWasClickedOnTheList() throws Exception {
        addUnitToBattle("TestUnit");
        selectFirstUnit();
        WaitForAsyncUtils.waitForFxEvents();

        DataObject wand = testDataProvider.getEquipment("Old wand");

        assertTrue(chosenUnitEquipmentListView.getItems().contains(wand));
    }

    @Test
    public void ChosenUnitAttacksList_shouldContainCertainUnitAttacks_afterUnitWasClickedOnTheList() throws Exception {
        addUnitToBattle("TestUnit");
        selectFirstUnit();
        WaitForAsyncUtils.waitForFxEvents();

        DataObject kick = testDataProvider.getAttack("Kick");

        assertTrue(chosenUnitAttacksListView.getItems().contains(kick));
    }

    @Test
    public void AdditionalInfoList_shouldDisplayAttackInformation_afterAttackWasClicked() throws Exception {
        addUnitToBattle("TestUnit");
        selectFirstUnit();
        Platform.runLater(() -> chosenUnitAttacksListView.getSelectionModel().selectFirst());
        WaitForAsyncUtils.waitForFxEvents();

        List<String> attackInfo = new ArrayList<>(Arrays.asList("Description: A normal kick."));
        assertEquals(attackInfo, additionalInfoListView.getItems());
    }

    @Test
    public void AdditionalInfoList_shouldDisplayEquipmentInformation_afterEquipmentWasClicked() throws Exception {
        addUnitToBattle("TestUnit");
        selectFirstUnit();
        Platform.runLater(() -> chosenUnitEquipmentListView.getSelectionModel().selectFirst());
        WaitForAsyncUtils.waitForFxEvents();

        List<String> attackInfo = new ArrayList<>(Arrays.asList("Description: An old wand from ancient times."));
        assertEquals(attackInfo, additionalInfoListView.getItems());
    }

    @Test
    public void AdditionalInfoList_shouldDisplaySkillInformation_afterSkillWasClicked() throws Exception {
        addUnitToBattle("TestUnit");
        selectFirstUnit();
        Platform.runLater(() -> chosenUnitSkillsListView.getSelectionModel().selectLast());
        WaitForAsyncUtils.waitForFxEvents();

        List<String> attackInfo = new ArrayList<>(Arrays.asList("Description: A healing energy that restores 10 hp to an ally."));
        assertEquals(attackInfo, additionalInfoListView.getItems());
    }

    @Test
    public void AdditionalInfoList_shouldDisplaySkillInformation_afterClickingSkillEquipmentSkill() throws Exception {
        addUnitToBattle("TestUnit");
        selectFirstUnit();
        Platform.runLater(() -> chosenUnitSkillsListView.getSelectionModel().selectLast());
        Platform.runLater(() -> chosenUnitEquipmentListView.getSelectionModel().selectFirst());
        Platform.runLater(() -> chosenUnitSkillsListView.getSelectionModel().selectLast());
        WaitForAsyncUtils.waitForFxEvents();
        List<String> attackInfo = new ArrayList<>(Arrays.asList("Description: A healing energy that restores 10 hp to an ally."));
        assertEquals(attackInfo, additionalInfoListView.getItems());
    }

    @Test
    public void AdditionalInfoList_shouldDisplaySkillInformation_afterClickingSkillAttackSkill() throws Exception {
        addUnitToBattle("TestUnit");
        selectFirstUnit();
        Platform.runLater(() -> chosenUnitSkillsListView.getSelectionModel().selectLast());
        Platform.runLater(() -> chosenUnitAttacksListView.getSelectionModel().selectFirst());
        Platform.runLater(() -> chosenUnitSkillsListView.getSelectionModel().selectLast());
        WaitForAsyncUtils.waitForFxEvents();

        List<String> attackInfo = new ArrayList<>(Arrays.asList("Description: A healing energy that restores 10 hp to an ally."));
        assertEquals(attackInfo, additionalInfoListView.getItems());
    }

    @Test
    public void AdditionalInfoList_shouldDisplayEquipmentInformation_afterClickingEquipmentSkillEquipment() throws Exception {
        addUnitToBattle("TestUnit");
        selectFirstUnit();
        Platform.runLater(() -> chosenUnitEquipmentListView.getSelectionModel().selectFirst());
        Platform.runLater(() -> chosenUnitSkillsListView.getSelectionModel().selectFirst());
        Platform.runLater(() -> chosenUnitEquipmentListView.getSelectionModel().selectFirst());
        WaitForAsyncUtils.waitForFxEvents();

        List<String> attackInfo = new ArrayList<>(Arrays.asList("Description: An old wand from ancient times."));
        assertEquals(attackInfo, additionalInfoListView.getItems());
    }

    @Test
    public void AdditionalInfoList_shouldDisplayEquipmentInformation_afterClickingEquipmentAttackEquipment() throws Exception {
        addUnitToBattle("TestUnit");
        selectFirstUnit();
        Platform.runLater(() -> chosenUnitEquipmentListView.getSelectionModel().selectFirst());
        Platform.runLater(() -> chosenUnitAttacksListView.getSelectionModel().selectFirst());
        Platform.runLater(() -> chosenUnitEquipmentListView.getSelectionModel().selectFirst());

        List<String> attackInfo = new ArrayList<>(Arrays.asList("Description: An old wand from ancient times."));
        WaitForAsyncUtils.waitForFxEvents();
        assertEquals(attackInfo, additionalInfoListView.getItems());
    }

    @Test
    public void AdditionalInfoList_shouldDisplayAttackInformation_afterClickingAttackEquipmentAttack() throws Exception {
        addUnitToBattle("TestUnit");
        selectFirstUnit();
        Platform.runLater(() -> chosenUnitAttacksListView.getSelectionModel().selectFirst());
        Platform.runLater(() -> chosenUnitEquipmentListView.getSelectionModel().selectFirst());
        Platform.runLater(() -> chosenUnitAttacksListView.getSelectionModel().selectFirst());
        WaitForAsyncUtils.waitForFxEvents();

        List<String> attackInfo = new ArrayList<>(Arrays.asList("Description: A normal kick."));
        assertEquals(attackInfo, additionalInfoListView.getItems());
    }

    @Test
    public void AdditionalInfoList_shouldDisplayAttackInformation_afterClickingAttackSkillAttack() throws Exception {
        addUnitToBattle("TestUnit");
        selectFirstUnit();
        Platform.runLater(() -> chosenUnitAttacksListView.getSelectionModel().selectFirst());
        Platform.runLater(() -> chosenUnitSkillsListView.getSelectionModel().selectFirst());
        Platform.runLater(() -> chosenUnitAttacksListView.getSelectionModel().selectFirst());
        WaitForAsyncUtils.waitForFxEvents();

        List<String> attackInfo = new ArrayList<>(Arrays.asList("Description: A normal kick."));
        assertEquals(attackInfo, additionalInfoListView.getItems());
    }
}
