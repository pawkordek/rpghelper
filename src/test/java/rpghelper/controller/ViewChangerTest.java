package rpghelper.controller;

import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import org.junit.Test;
import org.testfx.util.WaitForAsyncUtils;
import rpghelper.helper.FxApplicationTest;
import rpghelper.model.Module;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ViewChangerTest extends FxApplicationTest {

    @Override
    public void init() throws Exception {
        setViewName("TestView");
    }

    @Test
    public void View_shouldChange() throws Exception {
        changeView();
        Button testButtonAfter = getGuiElement("#testButtonAfter");
        assertNotNull(testButtonAfter);
    }

    @Test
    public void TestControllerAfter_shouldHaveTheSameModuleAsTestController_afterViewWasChanged() throws Exception {
        Module moduleBeforeViewChange = currentController.getModule();
        changeView();
        ListView<Module> moduleTestListView = getGuiElement("#moduleTestListView");
        Module moduleAfterViewChange = moduleTestListView.getItems().get(0);
        assertEquals(moduleBeforeViewChange, moduleAfterViewChange);
    }

    private void changeView() throws Exception {
        Button testButton = getGuiElement("#testButton");
        ViewChanger viewChanger = new ViewChanger(testButton, currentController.getModule(), "/rpghelper/view/");
        Platform.runLater(() -> viewChanger.changeViewTo("TestViewAfter"));
        WaitForAsyncUtils.waitForFxEvents();
    }
}
