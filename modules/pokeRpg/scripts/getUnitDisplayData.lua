function addUnitData(...)
    for k, v in ipairs { ... } do
        table.insert(unitData, v)
    end
end

function addUnitListData(list)
    for i = 0, list:size() - 1, 1 do
        table.insert(unitData, list:get(i))
    end
end

function unitStringData(identifier)
    _unit:getStringData(identifier)
end

function unitListData(identifier)
    _unit:getListData(identifier)
end

function func(unit)
    _unit = unit
    unitData = {
        "Nr: " .. unit:getStringData("nr"),
        "Height: " .. unit:getStringData("height"),
        "Weight: " .. unit:getStringData("weight"),
        "Skills: ",
    }
    local skills = unit:getListData("skills")
    for i = 0, skills:size() - 1, 1 do
        table.insert(unitData, skills:get(i))
    end
    addUnitData("Catch rate: " .. unit:getStringData("catchRate"),
        "Growth rate: ")
    local growthRate = unitStringData("growthRate")
    local growthRateNr
    if growthRate == "Erratic" then
        growthRateNr = 0
    elseif growthRate == "Fast" then
        growthRateNr = 1
    elseif growthRate == "Medium Fast" then
        growthRateNr = 2
    elseif growthRate == "Medium Slow" then
        growthRateNr = 3
    elseif growthRate == "Slow" then
        growthRateNr = 4
    elseif growthRate == "Fluctuating" then
        growthRateNr = 5
    else
        growthRateNr = -1
    end
    addUnitData(growthRateNr, "Gender: ")
    if unit:getStringData("genderless") == "true" then
        addUnitData("genderless")
    else
        addUnitData(unit:getStringData("genderMaleChance") .. "% male")
    end
    stats = unit:getMapData("stats")
    addUnitData("Stats: ",
        "HP: " .. stats:get("hp"),
        "Attack: " .. stats:get("attack"),
        "Defense: " .. stats:get("defense"),
        "S.Attack: " .. stats:get("s.attack"),
        "S.Defense: " .. stats:get("s.defense"),
        "Speed: " .. stats:get("speed"),
        "Types: ")
    --[[    local types = unit:getListData("types")
        for i = 0, types:size() - 1, 1 do
            table.insert(unitData, types:get(i))
        end]]
    addUnitListData(unit:getListData("types"))
    addUnitData("Pokedex info:")
    addUnitListData(unit:getListData("pokedexInfo"))
    return unitData
end


