#RPG Helper
Application with a purpose of helping in preparing and running a roleplaying session.

##Application structure
Application will be split into three parts, battle menu, modification menu and
story menu. 

The purpose of the battle menu is to simplify and automate various calculations
and management done when creating encounters in roleplaying games.

The purpose of the modification menu is to allow to create, modify and remove
things like monsters or items etc. used in a game.

The purpose of story menu is to allow to have a access to all information about
players, world or story of a roleplaying campaign in one place. 

Application will use so called "modules" to store data specific to a certain
game which then can be chosen in the program.

Modules will contain data in JSON format. By using Lua language
(and possibly other ones as well in the future)
the user can decide which and how this data will be displayed 
and manipulate it in the program.

##Progress
###TODO:
- Finish Battle Menu
    - Allow to display attacks', skill's and equipment's statistics
    - Allow to add units to battle
    - Allow to run a battle
    - Allow to run various calculations, for example, damage
- Finish a launcher of Lua scripts
    - Make sure that every needed amount of parameters can be passed
    - Make sure that every required return value can be returned
- Make Modifications menu
- Make Story menu
- [More to be added]
###DONE:
- Simple logging system
- JavaFX views switching
- Module choosing and loading
- Module choice menu
- Module storing all it's related data
- Loading the data from JSON
- Getting access to data as String, List or Map
- Displaying unit's statistics, attacks, skills and equipment
